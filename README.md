# Profiel voor specificaties iStandaarden

Het profiel is gerealiseerd voor Respec (https://github.com/w3c/respec)

## Voor development

Zie [developer guide](https://github.com/w3c/respec/wiki/Developers-Guide) om Respec te installeren.
kopieer vervolgens profiles/istandaarden.js profiel en de src/istandaarden/* naar de directory van respec-dev. Zet de builds ook in de builds directory van respec-dev. 

In de basic.html van de examples moet de volgende regel aangepast worden.

Van

```html
<script src='../profiles/w3c.js' async class='remove'></script>
```

Naar

```html
<script src='../profiles/istandaarden.js' async class='remove'></script>
```

Installatie klaar. En ontwikkelen maar.

### Deployment

Werkt alles! Dan kun je builden: node ./tools/builder.js --profile=istandaarden

Vervolgens moet je de respec-profile kopieren vanuit je dev omgeving. Gebruik ./deploy_profile.sh voor het maken van de kopie. De respec-dev directory in het script moet je zelf aanpassen aan jouw omgeving. Vervolgens gebruik je ./ci/deploy_template.sh om de template samen te stellen.
