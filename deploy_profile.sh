#!/bin/bash
dev_directory="../respec-dev"

cp "$dev_directory"/assets/istandaarden*.css ci/template/assets/.
cp "$dev_directory"/assets/images/iStandaarden-*.svg ci/template/assets/images/.
cp "$dev_directory"/profiles/istandaarden.js profiles/.
cp -R "$dev_directory"/src/istandaarden src/.
cp "$dev_directory"/builds/respec-istandaarden.js ci/template/respec/respec-istandaarden.js
cp "$dev_directory"/builds/respec-istandaarden.js.map ci/template/respec/respec-istandaarden.js.map
cp "$dev_directory"/builds/respec-istandaarden.js builds/respec-istandaarden.js
cp "$dev_directory"/builds/respec-istandaarden.js.map builds/respec-istandaarden.js.map